# golang-containerize-example

Containerize Your Go Developer Environment

## Example 1
Based in:
* [Containerize Your Go Developer Environment](https://www.docker.com/blog/containerize-your-go-developer-environment-part-1/)

* Build a img with the app

```
docker build -t example1 .
```

* Build a binary with the image and left in `./bin` folder
```
docker build --target bin --output bin/ .
```

## Example 2
Based in:
* [Containerize Your Go Developer Environment](https://www.docker.com/blog/containerize-your-go-developer-environment-part-1/)

* Build a binary with the image and left in `./bin`
```
docker build --target bin --output bin/ --platform local .
```

### Some parameters used:
* `--platform` : The optional --platform flag can be used to specify the platform of the image in case FROM references a multi-platform image. For example, linux/amd64, linux/arm64, or windows/amd64 [Automatic plataform ARGs in global scope](https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope)
* [Multi plataform docker builds](https://www.docker.com/blog/multi-platform-docker-builds/)
